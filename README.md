# IMDB

## How to run the project on local
- Go to Project Directory
- Run `npm install` to install all the dependencies 
- To Run the project `npm run serve`
- The server is started in `http://127.0.0.1:8080/`

## To setup backend 
- To setup backend, clone this repository `https://balajirajendran321@bitbucket.org/balajirajendran321/imdb-assessment-backend`
- Update Database credentials in `appsettings.json` file
- To run the project `ctrl+f5`
