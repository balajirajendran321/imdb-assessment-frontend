import Movie from './screens/Movie';
import NewMovie from './screens/NewMovie';
import Actor from './screens/Actor';
import Header from './components/Header.vue';

export const routes = [{
        path: '',
        name: 'home',
        components: {
            default: Movie,
            'header-top': Header
        }
    }, {
        path: '/actors',
        name: 'actors',
        components: {
            default: Actor,
            'header-top': Header
        }
    }, {
        path: '/movies/add',
        name: 'newmovie',
        components: {
            default: NewMovie,
            'header-top': Header
        }
    }, {
        path: '/movies/edit/:id',
        name: 'editmovie',
        components: {
            default: NewMovie,
            'header-top': Header
        }
    },
    {
        path: '*',
        redirect: '/'
    }
];