import Vue from 'vue'
import VueRouter from 'vue-router';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css'
import axios from 'axios'
import BootstrapVue from 'bootstrap-vue'

import App from './App.vue'
import {
    routes
} from './routes';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

axios.defaults.baseURL = process.env.VUE_APP_URL

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(BootstrapVue)
Vue.prototype.$http = axios;

const vuetify = new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
});

const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app')